const fs = require("fs");
const apis = require("apisauce");

const ga = require("golos-addons");
const global = ga.global;
const golos = ga.golos;
const golosjs = golos.golos;

global.initApp("otkat");

golos.setWebsocket(global.CONFIG.golos_node);

if (global.CONFIG.chain_id) {
    golos.setChainId(global.CONFIG.chain_id);
}
if (global.CONFIG.prefix) {
    golos.setPrefix(global.CONFIG.prefix);
}

if (!global.CONFIG.preferredPayoutAsset) {
    global.CONFIG.preferredPayoutAsset = "GBG";
}

const log = global.getLogger("index");

const CONFIG = global.CONFIG;

let GBG = "GBG";
let GESTS = "GESTS";
let GOLOS = "GOLOS";
let CMCURL = "https://api.coinmarketcap.com/v1/ticker/golos/?convert=GBG";
let CMCATTR = "price_gbg";

if (CONFIG.GBG) {
    GBG = CONFIG.GBG
}

if (CONFIG.cmcurl) {
    CMCURL = CONFIG.cmcurl;
}

if (CONFIG.cmc_price_attr) {
    CMCATTR = CONFIG.cmc_price_attr;
}

const USER = global.CONFIG.userid;
const KEY = global.CONFIG.key;
const PERCENT = global.CONFIG.voter_reward_percent;
const MEMO = global.CONFIG.memo;

const BROADCAST = global.broadcast;
let STRICT = true;

for(let val of process.argv) {
    if(val.match(/ignorecheck/)) {
        STRICT = false;
    }
}


async function getGolosGpgRatio() {
    const api = apis.create(
        {
            baseURL: CMCURL,
            headers: {
                'Cache-Control': 'no-cache'
            },
            timeout: 10000
        });

    try {
        const info = await api.get("");
        return info.data[0][CMCATTR];
    } catch (e) {
        log.error("Не получилось получить рыночное соотношение цены GBG к GOLOS");
    }
    

}

function getPermlink() {
    for(let val of process.argv) {
        let m = val.match(/^permlink=([^ ]+)$/);
        if(m) {
            return m[1];
        }
    }
    return null;
}

function usage() {
    log.info("usage: node index permlink=socrealizm-kotorogo-vy-ne-videli-7")
}

async function getContent(permlink) {
    const content = await golosjs.api.getContent(USER, permlink, -1);
    if(!content) {
        log.error("post for the permlink is not found!");
        process.exit(1);
    }    
    return content;
}


class Scanner {
    constructor(content) {
        this.content = content;
        this.starttime = Date.parse(content.created);
        this.author_reward = 0;
        this.reward_time = 0;
    }

    process(he) {
        const tr = he[1];
        const time = Date.parse(tr.timestamp);
        if(time < this.starttime) {
            return true;
        }
        const op = tr.op[0];
        const opBody = tr.op[1];

        switch(op) {
            case "author_reward":
                if(opBody.permlink == this.content.permlink) {
                    this.author_reward = parseFloat(opBody.sbd_payout.split(" ")[0]);
                    this.gests = opBody.vesting_payout;
                    this.golos = parseFloat(opBody.steem_payout.split(" ")[0]);
                    this.reward_time = time;
                }
                break;
        }
    }
}

async function collectInfos(content) {
    const scanner = new Scanner(content);
    await golos.scanUserHistory(USER, scanner);
    return scanner;
}

function sumRshares(content, reward_time) {
    let sum = 0;
    for (let v of content.active_votes) {
        if (Date.parse(v.time) > reward_time) {
            continue;
        }
        
        if(v.rshares > 0) {
            sum += v.rshares / 1000000;
        } let reward
    }
    return sum;
}

async function createTransferList(content, reward_gbg, reward_golos, ratio, reward_time, sum_rshares, memo) {
    const reward = reward_gbg + reward_golos * ratio;

    let sum_transfered = {
        GBG: 0,
        GOLOS: 0
    };
    let sumIncreased = {
        GBG: 0,
        GOLOS: 0
    };
    content.active_votes.sort((a, b) => { return b.rshares - a.rshares });
    
    const transfers = [];

    for (let v of content.active_votes) {
        const weight = parseInt(v.percent / 100);

        log.debug("user ", v.voter, weight);

        if (Date.parse(v.time) > reward_time) {
            log.debug("Поздний голос, после первых выплат", v.voter, v.time, Date(reward_time));
            continue;
        }

        log.debug("rshares = " + v.rshares);
        if (v.rshares <= 0) {
            log.debug("Игнорируем флаг или снятие голоса", v.voter, v.rhsares);
            continue;
        }

        if (CONFIG.minWeight > weight) {
            log.debug("Недостаточный вес голоса", v.voter, weight, CONFIG.minWeight);
            continue;
        }

        if (global.CONFIG.bypass.includes(v.voter)) {
            log.debug("Аккаунт в черном списке" , v.voter);
            continue;
        }

        // bypass own account
        if(global.CONFIG.userid == v.voter) {
            log.debug("Ингорируем свой голос:" , v.voter);
            continue;
        }

        let rshares = v.rshares / 1000000;

        if (CONFIG.applyVoteWeight) {
            rshares = rshares * weight / 100;
        }

        let payout = rshares * reward / sum_rshares;
        let payout_asset = GBG;
        if (CONFIG.preferredPayoutAsset == GBG) {
            if (reward_gbg < payout) {
                payout = payout / ratio;
                payout_asset = GOLOS;
                reward_golos -= payout;
            } else {
                reward_gbg -= payout;
            }
        } else {
            let new_payout = payout / ratio;
            if (reward_golos < new_payout) {
                payout_asset = GBG;
                reward_gbg -= payout;
            } else {
                payout = new_payout;
                payout_asset = GOLOS;
                reward_golos -= payout;
            }
        }

        if (payout < CONFIG.minReward) {
            if (CONFIG.minWeightForMinReward <= weight) {
                log.debug("Вознаграждение ниже порогового, устанавливаем в минимум", v.voter, CONFIG.minReward);
                sumIncreased[payout_asset] += CONFIG.minReward - payout;
                payout = CONFIG.minReward;
            } else {
                log.debug("Вознаграждение ниже порогового, недостаточный вес голоса", v.voter, weight, CONFIG.minWeightForMinReward);
            }
        }

        log.debug("user's payout", payout.toFixed(3) + " " + payout_asset);

        if (payout >= 0.001) {

            if (CONFIG.beneficiaries && CONFIG.beneficiaries[v.voter]) {
                log.info("Для пользователя сконфигурированы выгодополучатели", v.voter);
                let bsum = 0;
                for (let b in CONFIG.beneficiaries[v.voter]) {
                    bsum += CONFIG.beneficiaries[v.voter][b];
                }
                if (bsum > 100) {
                    log.error("Процент суммы выгодополучателей превышает 100 процентов", v.voter);
                }
                log.info("\tсумма к распределению среди выгодополучателей", payout.toFixed(3) + " " + payout_asset);
                for (let b in CONFIG.beneficiaries[v.voter]) {

                    const bpayout = payout * CONFIG.beneficiaries[v.voter][b] / 100;
                    const amount = bpayout.toFixed(3) + " " + payout_asset;
                    log.info("\t" + b, "получает", amount, CONFIG.beneficiaries[v.voter][b] + "%");
                    
                    sum_transfered[payout_asset] += bpayout;
                    transfers.push([
                        b,
                        v.rshares / 1000000,
                        rshares,
                        weight,
                        amount,
                        memo
                    ]);
                }
            } else {
                const amount = payout.toFixed(3) + " " + payout_asset;
                sum_transfered[payout_asset] += payout;
                transfers.push([
                    v.voter,
                    v.rshares / 1000000,
                    rshares,
                    weight,
                    amount,
                    memo
                ]);
            }
        }
    }
    
    log.info("Колличество получателей вознаграждения", transfers.length);
    log.info("Сумма к перечислению", sum_transfered[GBG].toFixed(3) + " " + GBG);
    log.info("Сумма к перечислению", sum_transfered[GOLOS].toFixed(3) + " " + GOLOS);

    try {
        await fs.writeFileSync(CONFIG.transferList, JSON.stringify(transfers,null, 4));
        log.info("создан файл со списком вознаграждений", CONFIG.transferList);
    } catch (e) {
        log.error("Не получилось записать файл со списком вознаграждений", CONFIG.transferList);
        log.error(e);
        process.exit(2);
    }        
}

async function getGolosGbgFeedRatio() {
    const { current_median_history } = await golosjs.api.getFeedHistoryAsync();
    const { base, quote } = current_median_history;
    const [base_amount, base_asset] = base.split(" ");
    const [quote_amount, quote_asset] = quote.split(" ");

    if (base_asset == "GBG") {
        return parseFloat(base_amount) / parseFloat(quote_amount);
    } else {
        return parseFloat(quote_amount) / parseFloat(base_amount);
    }
}

async function run() {


    let permlink = getPermlink();
    if(!permlink) {
        usage();
        process.exit(1);
    }
    log.info("Пользователь = " + USER);
    log.info("permlink = " + permlink);

    const content = await getContent(permlink);
    const infos = await collectInfos(content);

    let ratio = 1;
    if (global.CONFIG.useMarketRatio) {
        ratio = await getGolosGpgRatio();
        log.info("Рыночная стоимость 1 GOLOS в GBG (coinmarketcap)", ratio);
    } else {
        ratio = await getGolosGbgFeedRatio();
        log.info("Cтоимость 1 GOLOS в GBG (фиды делегатов)", ratio);
    }

    await golos.getCurrentServerTimeAndBlock();
    const gests = parseFloat(golos.convertVestingToGolos(infos.gests).split(" ")[0]);

    const gbg = infos.author_reward;
    const gests_gbg = (gests * ratio);
    const golos_gbg = (infos.golos * ratio);
    const sum_gbg = gbg + gests_gbg + golos_gbg ;
    log.info("Найдено вознаграждение за пост:"
        ,infos.author_reward.toFixed(3) + " " + GBG
        ,infos.golos.toFixed(3) + " " + GOLOS
        ,gests.toFixed(3) + " " + GESTS
    );

    log.info("Сумма полученного вознаграждения в GBG эквиваленте (СГ + GOLOS + GBG)",
        sum_gbg.toFixed(3) + " " + GBG
    );

    let reward = sum_gbg / 2;

    log.info("Сумма вознаграждения кураторам (50%)", reward.toFixed(3) + " " + GBG);
    
    reward =  reward * PERCENT / 100;

    log.info("Итоговая сумма вознаграждения кураторам",
        reward.toFixed(3) + " GBG (" + PERCENT + "%)");

    const gbg_percent = gbg * 100 / (gbg + golos_gbg);
    const golos_percent = golos_gbg * 100 / (gbg + golos_gbg);
    
    let reward_golos = infos.golos;
    let reward_gbg = reward - reward_golos * ratio;

    if (CONFIG.preferredPayoutAsset == "GBG") {
        reward_gbg = gbg;
        reward_golos = (reward - reward_gbg) / ratio;
    }

    log.info("Суммы к распределению"
        ,reward_gbg.toFixed(3) + " GBG"
        ,reward_golos.toFixed(3) + " GOLOS"
    );

    
    const sum_rshares = sumRshares(content, infos.reward_time);
    log.info("Mrshares всего " + sum_rshares);
    let url = "https://golos.io";
    if (CONFIG.url) {
        url = CONFIG.url;
    }
    const memo = MEMO + " " + `${url}/${content.parent_permlink}/@${content.author}/${content.permlink}`;
    log.info("Текст для заметки = " + memo);

    await createTransferList(content, reward_gbg, reward_golos, ratio, infos.reward_time, sum_rshares, memo);

    log.info("DONE");

    process.exit(0);
}

async function prompt() {

    var stdin = process.stdin,
        stdout = process.stdout;

    return new Promise(resolve => {
        stdin.resume();
        stdin.once('data', function (data) {
            resolve();
        });
    });
}

run();
