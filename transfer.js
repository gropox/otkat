const fs = require("fs");

const ga = require("golos-addons");
const global = ga.global;
const golos = ga.golos;
const golosjs = require("golos-js");

global.initApp("otkat");

golos.setWebsocket(global.CONFIG.golos_node);

if (global.CONFIG.chain_id) {
    golos.setChainId(global.CONFIG.chain_id);
}
if (global.CONFIG.prefix) {
    golos.setPrefix(global.CONFIG.prefix);
}

if (!global.CONFIG.preferredPayoutAsset) {
    global.CONFIG.preferredPayoutAsset = "GBG";
}

const CONFIG = global.CONFIG;

let GBG = "GBG";
let GOLOS = "GOLOS"

if (CONFIG.GBG) {
    GBG = CONFIG.GBG
}

if (CONFIG.GOLOS) {
    GOLOS = CONFIG.GOLOS
}

const log = global.getLogger("index");

const USER = global.CONFIG.userid;
const KEY = global.CONFIG.key;
const PERCENT = global.CONFIG.voter_reward_percent;
const MEMO = global.CONFIG.memo;

const BROADCAST = global.broadcast;
let STRICT = true;

async function getUserBalance() {
    const user = await golos.getAccount(USER);
    if(!user) {
        log.error("account " + USER + " does not exists!");
        process.exit(1);
    }

    return {
       [GBG] : parseFloat(user.sbd_balance.split(" ")[0]),
       [GOLOS] : parseFloat(user.balance.split(" ")[0])
    }
}

let OPERATIONS = [];

async function transfer(to, amount, memo) {
    const transfer = ["transfer", {
        from: global.CONFIG.userid,
        to,
        amount,
        memo
    }];
    OPERATIONS.push(transfer);
}

async function _prepareTransaction(tx) {
    
    const properties = await golosjs.api.getDynamicGlobalPropertiesAsync();
    const chainDate = new Date(properties.time + 'Z');
    const refBlockNum = properties.head_block_number - 3 & 0xFFFF;

    const block = await golosjs.api.getBlockAsync(properties.head_block_number - 2);
    const headBlockId = block.previous;
    return Object.assign({
        ref_block_num: refBlockNum,
        ref_block_prefix: new Buffer(headBlockId, 'hex').readUInt32LE(4),
        expiration: new Date(chainDate.getTime() + 60 * 1000)
        }, tx);
};

async function send(tx, privKeys) {
    var transaction = await _prepareTransaction(tx);
    log.debug('Signing transaction (transaction, transaction.operations)', transaction, transaction.operations);
    const signedTransaction = golosjs.auth.signTransaction(transaction, privKeys);
    log.debug('Broadcasting transaction (transaction, transaction.operations)', transaction, transaction.operations);
    if (BROADCAST) {
        await golosjs.api.broadcastTransactionSynchronousAsync(signedTransaction);
    } else {
        log.info("broadcast не включен, транзакция не отправлена", signedTransaction);
    }
};

async function commit() {
    if (OPERATIONS.length > 0) {

        for (let i = 0; i < 4; i++) {
            try {
                await send(
                    {
                        extensions: [],
                        operations: OPERATIONS
                    },
                    { "active": global.CONFIG.key });
                OPERATIONS = [];
                return;
            } catch (e) {
                if (i >= 2) {
                    log.error("Ошибка отправки транзакции", e);
                    process.exit(1);
                }
            }
        }
    }
}


async function doTransfers(TRANSFERS) {

    let counter = 0;
    for (let tr of TRANSFERS) {
        counter++;
        const [voter, rshares_net, rshares, weight, amount, memo] = tr;
        log.info("Перевод", voter, amount, memo);
        await transfer(voter, amount, memo);

        if (counter >= global.CONFIG.transfersPerTransaction) {
            await commit();
            counter = 0;
        }
    }
    await commit();

}


async function run() {

    const transferList = global.CONFIG.transferList;
    let TRANSFERS = [];
    try {
        if (fs.existsSync(transferList)) {
            TRANSFERS = JSON.parse(fs.readFileSync(transferList, "utf8"));
        } else {
            log.error("Список трансферов не найден", transferList);
            process.exit(1);
        }
    } catch (e) {
        console.error("Ошибка чтения списка к перечислению (" + transferList + ")");
        console.error(e);
        process.exit(1);
    }   

    log.info("Пользователь =",USER);

    const user_balance = await getUserBalance();
    log.info("Баланс на счету =",user_balance);
    const reward = {
        GBG: 0,
        GOLOS: 0
    }
    for (let tr of TRANSFERS) {
        const value = tr[4];
        const [amount, asset] = value.split(" ");
        reward[asset] += parseFloat(amount);
    }

    log.info("Сумма к перечислению",reward);
    
    if (reward[GBG] > 0 && reward[GBG] > user_balance[GBG]) {
        log.error("!!!!  Недостаточно средств к перечислению  !!!");
        if(STRICT) {
            process.exit(1);
        }
    }

    if (reward[GOLOS] > 0 && reward[GOLOS] > user_balance[GOLOS]) {
        log.error("!!!!  Недостаточно средств к перечислению  !!!");
        if (STRICT) {
            process.exit(1);
        }
    }

    log.info(`
    ${(!BROADCAST?"Broadcasting is not enabled! NO transfers. Add \"broadcast\" parameter":"")}
    Press any key to do transfer or Ctrl-C to terminate...
`);
        if(!BROADCAST) {
            // properly handle SIGINT (CTRL-C in docker), https://stackoverflow.com/questions/10021373/what-is-the-windows-equivalent-of-process-onsigint-in-node-js
            if (process.platform === "win32") {
              var rl = require("readline").createInterface({
                input: process.stdin,
                output: process.stdout
              });

              rl.on("SIGINT", function () {
                process.emit("SIGINT");
              });
            }

            process.on("SIGINT", function () {
              //graceful shutdown
              process.exit();
            });
            await prompt();
        } else {
            log.info("Broadcasting enabled, start transferring in 5 sec.");
            await global.sleep(5000);
        }    

    await doTransfers(TRANSFERS);
    log.info("DONE");
    process.exit(0);
}

async function prompt() {

    var stdin = process.stdin,
        stdout = process.stdout;

    return new Promise(resolve => {
        stdin.resume();
        stdin.once('data', function (data) {
            resolve();
        });
    });
}

run();